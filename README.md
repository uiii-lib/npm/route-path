# Route path

Helper for managing routes for React Router 6.x.

# Installation

> Register `@uiii-lib` package registry to npm
>
> ```
> npm config set @uiii-lib:registry https://gitlab.com/api/v4/packages/npm/
> ```

Install the package

```
npm install --save @uiii-lib/route-path
```

# Usage

Define routes

```ts
import { routePath } from '@uiii-lib/route-path';

export const entryRoutes = {
	app: () => routePath(`/*`),
	admin: () => routePath(`/admin/*`)
};

export const appRoutes = {
	home: () => routePath(`/`, entryRoutes.app()),
	search: (query: string) => routePath(`search/${query}`, entryRoutes.app()),
	searchInCategory: (query: string, category: string) => routePath(`${category}/serach/${query}`, entryRoutes.app()),
	user: () => routePath(`user`, entryRoutes.app())
};

export const adminRoutes = {
	...
}
```

Setup routing

```tsx
// index.tsx

import React from 'react';
import ReactDOM from 'react-dom';
import { Routes, Route } from 'react-router-dom';

// import pages
...

export const App = () => {
	return (
		<Routes>
			<Route path={routes.home().routePath} element={<HomePage />} />
			<Route path={routes.searchInCategory(":query", ":category").routePath} element={<SearchPage />} />
			<Route path={routes.search(":query").routePath} element={<SearchPage />} />
			<Route path={routes.user().routePath} element={<UserPage />} />
			<Route element={<NotFoundPage />} />
		</Routes>
	);
}

export const AdminApp = () => {
	...
}

ReactDOM.render(
	<Router>
		<Routes>
			<Route path={routes.app().routePath} element={<App />} />
			<ProtectedRoute path={routes.admin().routePath} element={<AdminApp />} />
		</Routes>
	</Router>,
	document.getElementById('root')
);
```

Use in links

```tsx
<Link to={routes.home().url}>Home</Link>
<Link to={routes.search("query").url}>Search "query"</Link>
```
