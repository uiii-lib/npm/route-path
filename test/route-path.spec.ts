import { routePath } from "../src";

describe("routePath", () => {
	it("should define route path", () => {
		expect(routePath("/").routePath).toBe("/");
		expect(routePath("/").url).toBe("/");

		expect(routePath("/*").routePath).toBe("/*");
		expect(routePath("/*").url).toBe("/");

		expect(routePath("route").routePath).toBe("route");
		expect(routePath("route").url).toBe("/route");

		expect(routePath("parent/*").routePath).toBe("parent/*");
		expect(routePath("parent/*").url).toBe("/parent");

		expect(routePath("route", routePath("parent/*")).routePath).toBe("route");
		expect(routePath("route", routePath("parent/*")).url).toBe("/parent/route");

		expect(routePath("/", routePath("parent/*")).routePath).toBe("/");
		expect(routePath("/", routePath("parent/*")).url).toBe("/parent");
	});
});
