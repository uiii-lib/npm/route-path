export interface RoutePath {
	routePath: string;
	absoluteRoutePath: string;
	url: string;
}

export function routePath(routePath: string, parentRoutePath?: RoutePath): RoutePath {
	const parentAbsoluteRoutePath = parentRoutePath?.absoluteRoutePath || '/*';
	const absoluteRoutePath = parentAbsoluteRoutePath.replace(/\*$/, routePath);
	const url = absoluteRoutePath
		.replace(/\*$/, '') // remove trailing * from route path expecting descendant routes
		.replace(/(?!^)\/+$/, ''); // remove trailing slashes except the single one

	return {
		routePath,
		absoluteRoutePath,
		url
	}
}
